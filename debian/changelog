libnet-duo-perl (1.02-3) unstable; urgency=medium

  * Team upload.
  * Add debian/tests/pkg-perl/smoke-env.
    Don't run automated/release tests during autopkgtests.
    (Closes: #1026338)
  * debian/watch: use uscan macros.
  * Declare compliance with Debian Policy 4.6.2.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Dec 2022 20:59:06 +0100

libnet-duo-perl (1.02-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.4.1, no changes needed.
  * Re-export upstream signing key without extra signatures.
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Repository.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Set Testsuite header for perl package.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Apply multi-arch hints. + libnet-duo-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 08 Dec 2022 23:00:14 +0000

libnet-duo-perl (1.02-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 11:41:04 +0100

libnet-duo-perl (1.02-1) unstable; urgency=medium

  * New upstream release.
    - Use pagination to retrieve users and integrations via the admin
      API.  (Closes: #924127)
  * Update to debhelper compatibility level V12.
    - Depend on debhelper-compat instead of using debian/compat.
  * Update to standards version 4.3.0 (no changes required).
  * Stop explicitly setting xz compression in debian/source/options.
  * Refresh upstream signing key.

 -- Russ Allbery <rra@debian.org>  Sat, 09 Mar 2019 14:36:00 -0800

libnet-duo-perl (1.01-2) unstable; urgency=medium

  [ Russ Allbery ]
  * Contribute the package to the Debian Perl Group.
    - Change Maintainer to the group.
    - Add myself to Uploaders.
    - Change Vcs-Git and Vcs-Browser to point to the pkg-perl repository.
  * Update to debhelper compatibility level V11.
  * Update to standards version 4.1.4.
    - Use https for debian/copyright URLs.
  * Use https for Homepage and debian/watch URL.
  * Add Rules-Requires-Root: no.
  * Run wrap-and-sort -ast.
  * Remove debian/gbp.conf and use the standard pkg-perl branch layout.
  * Refresh upstream signing key.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

 -- Russ Allbery <rra@debian.org>  Mon, 07 May 2018 16:59:34 -0700

libnet-duo-perl (1.01-1) unstable; urgency=medium

  * New upstream release.
    - Fixes FTBFS and JSON decoding with JSON::XS 3.0.  (Closes: #795734)
  * Prefer *.tar.xz in debian/watch to match packaging.
  * Add debian/gbp.conf reflecting the branch layout of the default
    packaging repository.
  * Refresh upstream signing key.
  * Update standards version to 3.9.6 (no changes required).

 -- Russ Allbery <rra@debian.org>  Sun, 16 Aug 2015 12:01:54 -0700

libnet-duo-perl (1.00-1) unstable; urgency=medium

  * Initial release.  (Closes: #754503)

 -- Russ Allbery <rra@debian.org>  Fri, 11 Jul 2014 13:01:30 -0700
